const files = {
  Blank: [
    'menu.vue',
    'routes.js',
    'store.js',
    'vue'
  ],
  Crud: [
    'edit.vue',
    'list.vue',
    'menu.vue',
    'routes.js',
    'store.js',
    'vue'
  ]
}

const actions = (answer) => {
  const copyActions = files[answer.contentType].map(item => {
    return {
      type: 'add',
      path: `src/components/Content/{{ name }}/{{ name }}.${ item }`,
      templateFile: `templates/Content/{{ contentType }}/{{ contentType }}.${ item }`
    }
  })

  const modifActions =
        [
          {
            type: 'modify',
            path: `src/components/Content/Content.menu.js`,
            pattern: /(\/\* x-menu \*\/)/,
            template: `,\n  '{{ name }}'$1`
          },
          {
            type: 'modify',
            path: `src/components/Content/Content.routes.js`,
            pattern: /(\/\* x-import-routes \*\/)/,
            template: `$1\nimport {{ name }} from './{{ name }}/{{ name }}.routes'`
          },
          {
            type: 'modify',
            path: `src/components/Content/Content.routes.js`,
            pattern: /(\/\* x-spread-children \*\/)/,
            template: `$1\n  ...{{ name }},`
          },
          {
            type: 'modify',
            path: `src/components/Content/Content.store.js`,
            pattern: /(\/\* x-import-store \*\/)/,
            template: `$1\nimport {{ name }} from './{{ name }}/{{ name }}.store'`
          },
          {
            type: 'modify',
            path: `src/components/Content/Content.store.js`,
            pattern: /(\/\* x-add-module \*\/)/,
            template: `$1\n  {{ name }},`
          }]

  return [...copyActions, ...modifActions]
}

const rootActions = (answer) => {
  const copyActions = ['routes.js', 'vue'].map(item => {
    return {
      type: 'add',
      path: `src/components/{{ name }}/{{ name }}.${item}`,
      templateFile: `templates/Root/Root.${item}`
    }
  })

  const modifActions = [
    {
      type: 'modify',
      path: `src/router.js`,
      pattern: /(\/\* x-import-routes \*\/)/,
      template: `$1\nimport {{ name }} from '@/{{ name }}/{{ name }}.routes'`
    },
    {
      type: 'modify',
      path: `src/router.js`,
      pattern: /(\/\* x-inject-routes \*\/)/,
      template: `$1\n  ...{{ name }}(),`
    }
  ]

  return [...copyActions, ...modifActions]
}


module.exports = ( plop ) => {

  plop.setGenerator('content', {

    description: 'Create a new content',

    prompts: [
      {
        type: 'list',
        name: 'contentType',
        message: 'What is your content type?',
        choices: [
          'Blank',
          'Crud'
        ]
      },
      {
        type: 'input',
        name: 'name',
        message: 'What is your content name?'
      }

    ],
    actions
  })

  plop.setGenerator('root', {

    description: 'Create a new root page',

    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your root page?'
      }
    ],
    actions: rootActions
  })
}

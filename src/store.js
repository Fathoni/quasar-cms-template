import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import qs from 'qs'
import router from './router.js'
import jwtDecode from 'jwt-decode'
import types from './types.js'
import Content from '@/Content/Content.store'

axios.defaults.baseURL = 'http://localhost:3030'
axios.defaults.paramsSerializer = function (params) {
  return qs.stringify(params, {
    arrayFormat: 'indices'
  })
}

axios.interceptors.request.use(function (config) {
  if (!config.url.includes('authentication')) {
    config.headers.Authorization = localStorage.getItem('accessToken')
  }
  return config
}, function (error) {
  return Promise.reject(error)
})

axios.interceptors.response.use(function (response) {
  if (response.code === 401) {
    window.location.href = '/'
  }

  return response
}, function (error) {
  return Promise.reject(error)
})

Vue.use(Vuex)

const state = {
  accessToken: '',
  exp: null,
  userId: null
}

const getters = {
  isAuthenticated (state) {
    let currentTime = Date.now() / 1000
    return (state.exp && (state.exp > currentTime))
  }
}

const mutations = {
  [types.TOKEN_CHANGED] (state, token) {
    state.accessToken = token
    localStorage.setItem('accessToken', token)
  },
  [types.TOKEN_EXTRACTED] (state, {exp, userId}) {
    state.exp = exp
    state.userId = userId
  }
}

const actions = {
  [types.INITIALISE_STORE] ({ dispatch }) {
    let accessToken = localStorage.getItem('accessToken')
    dispatch(types.EXTRACT_TOKEN, accessToken)
  },
  [types.DO_LOGOUT] ({ commit }) {
    localStorage.clear()

    commit(types.TOKEN_CHANGED, null)
    commit(types.TOKEN_EXTRACTED, {exp: null, userId: null})

    router.push('/login')
    // after logout
  },
  [types.EXTRACT_TOKEN] ({ commit }, token) {
    if (token && token !== 'null') {
      commit(types.TOKEN_CHANGED, token)

      const jwtExtracted = jwtDecode(token)
      commit(types.TOKEN_EXTRACTED, jwtExtracted)

      router.push('/content/dashboard')
    }
    // extract token
    // save expires state
    // save userId
  }
}

const modules = {
  Content
}

const strict = true

export default new Vuex.Store({
  strict,
  state,
  getters,
  mutations,
  actions,
  modules
})

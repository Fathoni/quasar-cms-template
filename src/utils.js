import createCrudStore from 'vuex-crud'
import { Loading, Toast } from 'quasar'

const parseList = (response) => {
  const { data } = response
  return data
}

const parseSingle = (response) => {
  return response
}

const createCrud = (resource, options) => createCrudStore({
  state: {
    reload: true,
    populate: [],
    pagination: {
      page: 0,
      entries: 0,
      rowsPerPage: 0
    },
    filtering: {
      field: '',
      terms: ''
    }
  },
  mutations: {
    setReload (state, reload) {
      state.reload = reload
    },
    setFiltering (state, filtering) {
      state.filtering = filtering
    }
  },
  actions: {
    doQuery ({ dispatch, state }, dataTableParams) {
      const action = 'fetchList'
      if (!dataTableParams) {
        return dispatch(action)
      }

      const { sorting, filtering, pagination } = dataTableParams
      let sort = sorting.dir !== 0 ? {
        $sort: {
          [sorting.field]: sorting.dir
        }
      } : {}

      let filter = (filtering.field && filtering.terms) ? {
        [filtering.field]: {
          $regex: filtering.terms,
          $options: 'i'
        }
      } : {}

      let params = {
        $limit: pagination.rowsPerPage,
        $skip: (pagination.page - 1) * pagination.rowsPerPage,
        ...sort,
        ...filter
      }

      if (state.populate.length) {
        params.$populate = state.populate
      }

      dispatch(action, { config: { params } })
    }
  },
  onFetchListStart () {
    Loading.show()
  },
  onFetchListSuccess (state, resp) {
    const { skip, limit, total } = resp
    state.reload = false

    state.pagination.entries = total
    state.pagination.page = skip / limit + 1
    state.pagination.rowsPerPage = limit
    Loading.hide()
  },
  onFetchListError (state, err) {
    Loading.hide()
  },
  onCreateStart () {
    Loading.show()
  },
  onUpdateStart () {
    Loading.show()
  },
  onDeleteStart () {
    Loading.show()
  },
  urlRoot: `${resource}`,
  resource,
  idAttribute: '_id',
  parseList,
  parseSingle,
  ...options
})

const toastPositive = (message) => {
  Toast.create.positive({
    html: message
  })
}

const toastNegative = (message) => {
  Toast.create.negative({
    html: message
  })
}

export {
  createCrud,
  toastPositive,
  toastNegative
}

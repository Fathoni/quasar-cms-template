export default () => [
  {
    path: '/login',
    component: () => import('./Login.vue')
  },
  {
    path: '*',
    redirect: '/login'
  }
]

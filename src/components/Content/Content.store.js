/* x-import-store */
import Dashboard from './Dashboard/Dashboard.store.js'

let modules = {
// eslint-disable-next-line
/* x-add-module */
  Dashboard
}

const state = {}

const getters = {}

const mutations = {}

const actions = {}

const namespaced = true

export default {
  namespaced,
  state,
  getters,
  mutations,
  actions,
  modules
}

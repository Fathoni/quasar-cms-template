/* x-import-routes */
import Dashboard from './Dashboard/Dashboard.routes'

const children = [
// eslint-disable-next-line
/* x-spread-children */
  ...Dashboard
]

export default () => [{
  path: '/content',
  component: () => import('./Content.vue'),
  meta: {
    requiresLogin: true
  },
  children
}]

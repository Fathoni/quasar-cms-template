import Vue from 'vue'
import VueRouter from 'vue-router'

/* x-import-routes */
import Login from '@/Login/Login.routes'
import Content from '@/Content/Content.routes'
import store from './store'

Vue.use(VueRouter)

const routes = [
// eslint-disable-next-line
/* x-inject-routes */
  ...Login(),
  ...Content()
]

const router = new VueRouter({
  /*
   * NOTE! VueRouter "history" mode DOESN'T works for Cordova builds,
   * it is only to be used only for websites.
   *
   * If you decide to go with "history" mode, please also open /config/index.js
   * and set "build.publicPath" to something other than an empty string.
   * Example: '/' instead of current ''
   *
   * If switching back to default "hash" mode, don't forget to set the
   * build publicPath back to '' so Cordova builds work again.
   */

  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresLogin) && !store.getters.isAuthenticated) {
    // You can use store variable here to access globalError or commit mutation
    next('/login')
  }
  else {
    next()
  }
})

export default router

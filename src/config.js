const usedComponents = [
  'QLayout',
  'QToolbar',
  'QToolbarTitle',
  'QSearch',
  'QTabs',
  'QRouteTab',
  'QBtn',
  'QIcon',
  'QItemSide',
  'QItemMain',
  'QSideLink',
  'QListHeader',
  'QCollapsible',
  'QPopover',
  'QList',
  'QItem',
  'QScrollArea',
  'QCheckbox',
  'QCard',
  'QCardTitle',
  'QCardMain',
  'QCardMedia',
  'QCardSeparator',
  'QCardActions',
  'QDataTable',
  'QInput',
  'QUploader',
  'QTransition'
]

const dataTableConfig = {
  refresh: true,
  noHeader: false,
  columnPicker: true,
  selection: 'single',
  bodyStyle: {
    maxHeight: '500px'
  },
  rowHeight: '60px',
  responsive: true,
  pagination: {
    rowsPerPage: 10,
    options: [5, 10, 15, 30, 50, 500]
  }
}

export {
  dataTableConfig,
  usedComponents
}

import { Loading } from 'quasar'
import { createCrud, toastPositive, toastNegative } from 'src/utils'

const store = createCrud('{{ kebabCase name }}', {
  onCreateSuccess (state, response) {
    Loading.hide()
    toastPositive('{{ name }} created successfully')
  },
  onUpdateSuccess (state, response) {
    Loading.hide()
    toastPositive('{{ name }} updated successfully')
  },
  onDeleteSuccess (state, response) {
    Loading.hide()
    toastPositive('{{ name }} deleted successfully')
  },
  onCreateError (state, response) {
    Loading.hide()
    toastNegative('Failed to create {{ name }}')
  },
  onUpdateError (state, response) {
    Loading.hide()
    toastNegative('Failed to update {{ name }}')
  },
  onDeleteError (state, response) {
    Loading.hide()
    toastNegative('Failed to update {{ name }}')
  }

})

export default store

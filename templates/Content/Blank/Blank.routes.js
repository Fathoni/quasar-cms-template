export default [
  {
    path: '{{ kebabCase name }}',
    component: () => import('./{{ name }}.vue')
  }
]
